import React from 'react';

import { Routes, Route } from "react-router-dom"

import Certificate from '../components/pages/Certificate';
import Home from '../components/pages/Home';
import MintPage from '../components/pages/MintPage';

import './App.css'

export default function App() {
    //<Certificate/>
    return ( 
        <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/certificate" element={ <Certificate/> } />
            <Route path="mint" element={ <MintPage/> } />
        </Routes>
    )
}