import React from "react";
import { Link } from "react-router-dom";

export default function Home(){
    return(
        <>
            <Link to='/certificate'>Certificate</Link>
            <Link to='/mint'>MintPage</Link>
        </>
    )
}